# Microservicio creacion y consulta de usuarios
***
El proyecto es un microservicio de creacion y consulta de usuarios.
Expone una api rest para la creación de un usuario, el cual es almacenado en una base de datos in memory (H2)
El usuario creado es autenticado y se devuelve la información de el nuevo usuario con un token (JWT) de autenticación.

## Technologias
***
El proyecto hace uso de las siguientes tecnologías:
* [Java] Version 1.8
* [Spring Framework Boot] Version 2.7.6
* [Gradle]
* [H2]
* [JUnit]
* [Mockito]
* [Model Mapper]
* [Lombok]

## Instalacion
***
. Para ejecutar el proyecto en la máquina local se requiere contar con JDK 8 y gradle 7 o superior
```
$ git clone https://gitlab.com/PabloCocinero/usersproject.git
$ cd evaluation
$ gradle wrapper --gradle-version 7.6 //colocar el numero de version instalada
$ ./gradlew build
$ ./gradlew bootRun
```
También es posible abrir el proyecto con Spring Tool Suit o Intellij y ejecutarlo a través del IDE

## Pruebas
***
Importar en postman el archivo Challenge BCI.postman_collection.json ubicado en la carpeta raiz del proyecto