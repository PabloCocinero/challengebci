package com.bci.challenge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.bci.challenge.configuration.TokenManager;
import com.bci.challenge.dto.ResponseDTO;
import com.bci.challenge.dto.UserDTO;
import com.bci.challenge.dto.UserResponseDTO;
import com.bci.challenge.model.User;
import com.bci.challenge.repository.UserRepository;
import com.bci.challenge.service.impl.UserServiceImpl;
import com.bci.challenge.util.ResponseDTOConverter;
import com.bci.challenge.util.UserDTOConverter;
import com.bci.challenge.util.Validator;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

	@InjectMocks
	private UserServiceImpl userServiceImpl;
	
	@Mock
	private UserRepository userRepository;
	@Mock
	private UserDTOConverter userDTOConverter;
	@Mock
	private ResponseDTOConverter responseDTOConverter;
	@Mock
	private TokenManager tokenManager;
	@Mock
	private PasswordEncoder passwordEncoder;
	private MockedStatic<Validator> mocked;
	
	private UserDTO userDTO;
	private UserResponseDTO userResponseDTO;
	private User user, fullUser;
	private ArrayList<User> userList, emptyList;
	private ResponseDTO responseDTO, responseDTOwithJWT, loginResponse;
	private LocalDateTime now;
	private String token, responseToken, authorization;
	
	public void userServiceImplTest() {
	}
	
	@BeforeEach
	public void setUp() {
		now = LocalDateTime.now();
		authorization = "Bearer eyJhbGciOiJIUzM4NCJ9.eyJhdXRob3JpdGllcyI6InVzZXIiLCJ1c2VybmFtZSI6InBhYmxvZGFuaWVsQGdtYWlsLmNvbSIsInN1YiI6IkpXVCBUb2tlbiIsImlzcyI6IlBhYmxvIiwiaWF0IjoxNjc1MzYyMTM0LCJleHAiOjE2NzUzOTIxMzR9.sNZYr3OrJRi9e3mLTlkz_WDGJJX36k8a_XJcRyvVK-BVVuIzGrYj891BKPL1ToYt";
		token = "eyJhbGciOiJIUzM4NCJ9.eyJhdXRob3JpdGllcyI6InVzZXIiLCJ1c2VybmFtZSI6InBhYmxvZGFuaWVsQGdtYWlsLmNvbSIsInN1YiI6IkpXVCBUb2tlbiIsImlzcyI6IlBhYmxvIiwiaWF0IjoxNjc1MzYyMTM0LCJleHAiOjE2NzUzOTIxMzR9.sNZYr3OrJRi9e3mLTlkz_WDGJJX36k8a_XJcRyvVK-BVVuIzGrYj891BKPL1ToYt";
		responseToken = "eyJhbGciOiJIUzM4NCJ9.eyJhdXRob3JpdGllcyI6InVzZXIiLCJ1c2VybmFtZSI6InBhYmxvZGFuaWVsQGdtYWlsLmNvbSIsInN1YiI6IkpXVCBUb2tlbiIsImlzcyI6IlBhYmxvIiwiaWF0IjoxNjc1MzYyMTc0LCJleHAiOjE2NzUzOTIxNzR9.e6CQssWXxrzudCStkDnFI6J3zCR2WB3GzuC5Z-oXSt-CIV3hNd3NLWefAXViJWgA";
		userDTO = new UserDTO(null,"pablodaniel@gmail.com","Afg5hjk7",null);
		userResponseDTO = new UserResponseDTO(null,"pablodaniel@gmail.com","Afg5hjk7",null);
		user = new User();
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		fullUser = new User();
		fullUser.setEmail(userDTO.getEmail());
		fullUser.setPassword(userDTO.getPassword());
		fullUser.setCreated(now);
		fullUser.setLastLogin(now);
		fullUser.setIsActive(true);
		fullUser.setToken(token);
		emptyList = new ArrayList<>();
		userList = new ArrayList<>();
		userList.add(fullUser);
		UUID uuid = UUID.randomUUID();
		responseDTO = new ResponseDTO(userResponseDTO,uuid,now,now,null,true,null);
		responseDTOwithJWT  = new ResponseDTO(userResponseDTO,uuid,now,now,token,true,null);
		mocked = mockStatic(Validator.class);
		loginResponse = new ResponseDTO(userResponseDTO, uuid, now, LocalDateTime.now(), responseToken, true, null);
	}
	
	@AfterEach
	public void after() {
		mocked.close();
	}
	
	@Test
	@DisplayName("testSaveUser")
	public void testCase_1() {
		mocked.when(() -> Validator.validateEmail(userDTO.getEmail())).thenReturn(true);
		mocked.when(() -> Validator.validatePassword(userDTO.getPassword())).thenReturn(true);
		when(passwordEncoder.encode(userDTO.getPassword())).thenReturn(userDTO.getPassword());
		when(userDTOConverter.toEntity(userDTO)).thenReturn(user);
		when(userRepository.save(user)).thenReturn(fullUser);
		when(responseDTOConverter.toDTO(fullUser)).thenReturn(responseDTOwithJWT);
		when(tokenManager.generateJwtToken(userDTO.getEmail())).thenReturn("token");
		
		assertEquals(responseDTOwithJWT,userServiceImpl.saveUser(userDTO,now));
	}
	
	@Test
	@DisplayName("loginOkTest")
	public void testCase_2() {
		when(tokenManager.validateJwtToken(token)).thenReturn(userDTO.getEmail());
		when(userRepository.findByEmail(userDTO.getEmail())).thenReturn(userList);
		when(tokenManager.generateJwtToken(userDTO.getEmail())).thenReturn(responseToken);
		when(userRepository.save(any(User.class))).thenReturn(fullUser);
		when(responseDTOConverter.toDTO(fullUser)).thenReturn(loginResponse);
		
		ResponseDTO response = userServiceImpl.login(authorization);
		
		assertNotNull(response);
		assertEquals(response.getUserDTO().getEmail(),userDTO.getEmail());
		assertEquals(response.getUserDTO().getPassword(),userDTO.getPassword());
		assertNotEquals(response.getCreated(),response.getLastLogin());
		assertNotEquals(response.getToken(),token);
	}
	@Test
	@DisplayName("loginFailUserNotExistTest")
	public void testCase_3() {
			when(tokenManager.validateJwtToken(token)).thenReturn(userDTO.getEmail());
			when(userRepository.findByEmail(userDTO.getEmail())).thenReturn(emptyList);
			
			assertThrows(BadCredentialsException.class,() -> userServiceImpl.login(authorization));
	}
	@Test
	@DisplayName("loginFailTokenNotEqualTest")
	public void testCase_4() {
			when(tokenManager.validateJwtToken(responseToken)).thenReturn(userDTO.getEmail());
			when(userRepository.findByEmail(userDTO.getEmail())).thenReturn(userList);
			
			assertThrows(BadCredentialsException.class,() -> userServiceImpl.login("Bearer " + responseToken));
	}
}
