package com.bci.challenge;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bci.challenge.util.Validator;

public class ValidatorTest {

	private String email, email1, email2, email3, password, password1, password2, password3;
	
	@BeforeEach
	public void setUp() {
		email = "pablo@gmail.com";
		email1 = "pablo@mail";
		email2 = "pablo.cocinero@mail123.com";
		email3 = "pablo.gmail.com";
		password = "Abc1d2ef";
		password1 = "abc78DEfgh";
		password2 = "abcdefghigKl";
		password3 = "abcD123ef";
	}
	
	@Test
	@DisplayName("testCorrectPassword")
	public void testCase_1() {
		assertTrue(Validator.validatePassword(password));
	}
	
	@Test
	@DisplayName("testPassword2Capitals")
	public void testCase_2() {
		assertFalse(Validator.validatePassword(password1));
	}
	@Test
	@DisplayName("testPasswordWithoutNumbers")
	public void testCase_3() {
		assertFalse(Validator.validatePassword(password2));
	}
	@Test
	@DisplayName("testPasswordWith3Numbers")
	public void testCase_4() {
		assertFalse(Validator.validatePassword(password3));
	}
	
	@Test
	@DisplayName("testCorrectEmail")
	public void testCase_5() {
		assertTrue(Validator.validateEmail(email));
	}
	
	@Test
	@DisplayName("testCorrectEmail")
	public void testCase_6() {
		assertTrue(Validator.validateEmail(email1));
	}
	
	@Test
	@DisplayName("testCorrectEmail")
	public void testCase_7() {
		assertTrue(Validator.validateEmail(email2));
	}
	
	@Test
	@DisplayName("testEmailWithout@")
	public void testCase_8() {
		assertFalse(Validator.validateEmail(email3));
	}
	
}
