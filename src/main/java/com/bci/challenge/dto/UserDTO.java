package com.bci.challenge.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserDTO {

	private String name;
	@NonNull
	private String email;
	@NonNull
	private String password;
	@JsonInclude(Include.NON_EMPTY)
	private List<PhoneDTO> phones;
}
