package com.bci.challenge.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class PhoneDTO {

	private Long number;
	private Integer citycode;
	private String countrycode;
}
