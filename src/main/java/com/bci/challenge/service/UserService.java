package com.bci.challenge.service;

import java.time.LocalDateTime;

import com.bci.challenge.dto.ResponseDTO;
import com.bci.challenge.dto.UserDTO;

public interface UserService {

	ResponseDTO saveUser(UserDTO userDTO, LocalDateTime now);
	
	ResponseDTO login(String authorization);
}
