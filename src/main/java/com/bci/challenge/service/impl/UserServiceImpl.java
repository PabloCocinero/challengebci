package com.bci.challenge.service.impl;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bci.challenge.configuration.TokenManager;
import com.bci.challenge.dto.ResponseDTO;
import com.bci.challenge.dto.UserDTO;
import com.bci.challenge.exception.ValidationException;
import com.bci.challenge.model.Phone;
import com.bci.challenge.model.User;
import com.bci.challenge.repository.UserRepository;
import com.bci.challenge.service.UserService;
import com.bci.challenge.util.Constants;
import com.bci.challenge.util.ResponseDTOConverter;
import com.bci.challenge.util.UserDTOConverter;
import com.bci.challenge.util.Validator;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserDTOConverter userDTOConverter;
	@Autowired
	private ResponseDTOConverter responseDTOConverter;
	@Autowired
	private TokenManager tokenManager;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public ResponseDTO saveUser(UserDTO userDTO, LocalDateTime now) {

		ResponseDTO ResponseDTO = new ResponseDTO();
		if (Validator.validateEmail(userDTO.getEmail())) {
			if(Validator.validatePassword(userDTO.getPassword())) {
			User user = userDTOConverter.toEntity(userDTO);
			if(nonNull(user.getPhones()) && !user.getPhones().isEmpty()) {
			for(Phone phone : user.getPhones()) {
				phone.setUser(user);
			}
			}
			user.setCreated(now);
			user.setLastLogin(now);
			user.setIsActive(true);
			user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
			user.setToken(tokenManager.generateJwtToken(userDTO.getEmail()));
			user = userRepository.save(user);
			ResponseDTO = responseDTOConverter.toDTO(user);
			ResponseDTO.getUserDTO().setPassword(null);
			}else {
				throw new ValidationException(Constants.BAD_PASSWORD_ERROR_MESSAGE);
			}
		} else {
			throw new ValidationException(Constants.BAD_EMAIL_ERROR_MESSAGE);
		}
		return ResponseDTO;
	}

	@Override
	public ResponseDTO login(String authorization) {

		String jwt = authorization.substring(7);
		String username = tokenManager.validateJwtToken(jwt); 
		List<User> users = userRepository.findByEmail(username);
		if(isNull(users) || users.isEmpty()){
			throw new BadCredentialsException("Token invalido");
		}
		User user = users.get(0);
		if (!user.getToken().equals(jwt)) {
			throw new BadCredentialsException("Token invalido");
		}
		user.setToken(tokenManager.generateJwtToken(username));
		user.setLastLogin(LocalDateTime.now());
		user = userRepository.save(user);
		ResponseDTO ResponseDTO = responseDTOConverter.toDTO(user);
		ResponseDTO.getUserDTO().setPassword(null);
		return ResponseDTO;
	}
}
