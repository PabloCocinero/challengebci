package com.bci.challenge.configuration;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@Component
public class TokenManager implements Serializable {

	private static final long serialVersionUID = 2829946167464615928L;
	public static final long TOKEN_VALIDITY = 30000000;
	
	@Value("${secret}")
	private String jwtSecret;
	private SecretKey jwtKey;

	public String generateJwtToken(String username) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("username", username);
		claims.put("authorities", "user");
		jwtKey = Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8));
		return Jwts.builder()
				.setClaims(claims)
				.setSubject("JWT Token")
				.setIssuer("Pablo")
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + TOKEN_VALIDITY))
				.signWith(jwtKey).compact();
	}

	public String validateJwtToken(String token) {

		Claims claims = getClaimsFromToken(token);
		String tokenUsername = (String) claims.get("username");
		Boolean isTokenExpired = claims.getExpiration().before(new Date());
		if(isTokenExpired)
			throw new BadCredentialsException("Token invalido");
		else
			return tokenUsername;
	}
	
	public Claims getClaimsFromToken(String token) {
		jwtKey = Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8));
		return Jwts.parserBuilder()
				.setSigningKey(jwtKey)
				.build()
				.parseClaimsJws(token)
				.getBody();
	}
}
