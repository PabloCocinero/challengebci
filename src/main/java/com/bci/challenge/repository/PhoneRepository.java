package com.bci.challenge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bci.challenge.model.Phone;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, Integer>{

}
