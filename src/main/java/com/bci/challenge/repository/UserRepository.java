package com.bci.challenge.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bci.challenge.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

	public List<User> findByEmail(String email);
}
