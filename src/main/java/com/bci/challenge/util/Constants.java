package com.bci.challenge.util;

public class Constants {

	public static final String BAD_EMAIL_ERROR_MESSAGE = "El email no tiene el formato correcto";
	public static final String BAD_PASSWORD_ERROR_MESSAGE = "El password no tiene el formato correcto";
	public static final String GENERIC_ERROR_MESSAGE = "Ha ocurrido un error"; 
	public static final String DATA_INTEGRITY_ERROR_MESSAGE = "Error de datos, puede que el email ingresado ya este registrado como usuario"; 
}
