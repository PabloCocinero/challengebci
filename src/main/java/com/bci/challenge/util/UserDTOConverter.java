package com.bci.challenge.util;

import org.springframework.stereotype.Component;

import com.bci.challenge.dto.UserDTO;
import com.bci.challenge.model.User;

@Component
public class UserDTOConverter extends Converter{

	public User toEntity(UserDTO userDTO) {
		return (User) super.fromTo(userDTO, User.class);
	}
	
	public User toEntity(UserDTO userDTO, User user) {
		return (User) super.fromTo(userDTO, user);
	}
	
	public UserDTO toDTO(User user) {
		return (UserDTO) super.fromTo(user,UserDTO.class);
	}
	
}