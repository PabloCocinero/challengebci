package com.bci.challenge.util;

import org.springframework.stereotype.Component;

import com.bci.challenge.dto.ResponseDTO;
import com.bci.challenge.model.User;

@Component
public class ResponseDTOConverter extends Converter{

	public ResponseDTO toDTO(User user) {
		return (ResponseDTO) super.fromTo(user, ResponseDTO.class);
	}
}
