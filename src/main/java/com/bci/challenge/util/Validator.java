package com.bci.challenge.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

	public static Boolean validateEmail(String email) {

		String regex = "^[A-Za-z0-9-+_.]+@[A-Za-z0-9.]+$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static Boolean validatePassword(String password) {
		String regexGral = "^[A-Z{1}0-9{2}a-z]{8,12}";
		String reg2numbers = "(\\D*\\d\\D*){2}";
		Pattern patternGral = Pattern.compile(regexGral);
		Pattern pattern2numbers = Pattern.compile(reg2numbers);
		Matcher matcherGral = patternGral.matcher(password);
		Matcher matcher2numbers = pattern2numbers.matcher(password);
		Boolean justOneCapital = password.chars().filter(Character::isUpperCase).count() == 1;
		return justOneCapital && matcherGral.matches() && matcher2numbers.matches();
	}
}
