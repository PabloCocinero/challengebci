package com.bci.challenge.util;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.bci.challenge.dto.PhoneDTO;
import com.bci.challenge.model.Phone;

@Component
public class PhoneDTOConverter extends Converter {

	public Phone toEntity(PhoneDTO phoneDTO) {
		return (Phone) super.fromTo(phoneDTO, Phone.class);
	}
	
	public Phone toEntity(PhoneDTO phoneDTO, Phone phone) {
		return (Phone) super.fromTo(phoneDTO, phone);
	}
	
	public PhoneDTO toDTO(Phone phone) {
		return (PhoneDTO) super.fromTo(phone, PhoneDTO.class);
	}
	
	public List<Phone> toEntityList(List<PhoneDTO> phones){
		return phones.stream().map(phone -> toEntity(phone)).collect(Collectors.toList());
	}
}