package com.bci.challenge.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ValidationException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	@Getter
	private final Integer code = 01;
	@Getter
	private final String detail;
}
