package com.bci.challenge.exception;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bci.challenge.dto.ErrorResponseDTO;
import com.bci.challenge.dto.ResponseDTO;
import com.bci.challenge.util.Constants;

import io.jsonwebtoken.JwtException;

@ControllerAdvice
public class CustomHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseDTO> handleAnyException(Exception exception){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<ErrorResponseDTO> errorList = new ArrayList<>();
		errorList.add(new ErrorResponseDTO(LocalDateTime.now(),02,Constants.GENERIC_ERROR_MESSAGE));
		responseDTO.setError(errorList);
		return new ResponseEntity<>(responseDTO,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(ValidationException.class)
	public ResponseEntity<ResponseDTO> handleValidationException(ValidationException validationException){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<ErrorResponseDTO> errorList = new ArrayList<>();
		errorList.add(new ErrorResponseDTO(LocalDateTime.now(),validationException.getCode(),validationException.getDetail()));
		responseDTO.setError(errorList); 
		return new ResponseEntity<>(responseDTO,HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<ResponseDTO> handleValidationException(BadCredentialsException badCredentialsException){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<ErrorResponseDTO> errorList = new ArrayList<>();
		errorList.add(new ErrorResponseDTO(LocalDateTime.now(),3,badCredentialsException.getLocalizedMessage()));
		responseDTO.setError(errorList); 
		return new ResponseEntity<>(responseDTO,HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(JwtException.class)
	public ResponseEntity<ResponseDTO> handleValidationException(JwtException jwtException){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<ErrorResponseDTO> errorList = new ArrayList<>();
		errorList.add(new ErrorResponseDTO(LocalDateTime.now(),3,jwtException.getLocalizedMessage()));
		responseDTO.setError(errorList); 
		return new ResponseEntity<>(responseDTO,HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ResponseDTO> handleConstraintException(DataIntegrityViolationException dataIntegrityViolationException){
		ResponseDTO responseDTO = new ResponseDTO();
		ArrayList<ErrorResponseDTO> errorList = new ArrayList<>();
		errorList.add(new ErrorResponseDTO(LocalDateTime.now(),04,Constants.DATA_INTEGRITY_ERROR_MESSAGE));
		responseDTO.setError(errorList); 
		return new ResponseEntity<>(responseDTO,HttpStatus.BAD_REQUEST);
	}
}
