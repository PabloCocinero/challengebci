package com.bci.challenge.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bci.challenge.dto.ResponseDTO;
import com.bci.challenge.dto.UserDTO;
import com.bci.challenge.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/sign-up")
	public ResponseEntity<ResponseDTO> signUp(@RequestBody @Validated UserDTO userDTO) {

		ResponseDTO responseDTO = userService.saveUser(userDTO, LocalDateTime.now());
		return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.CREATED);
	}

	@GetMapping("/login")
	public ResponseEntity<ResponseDTO> login(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization) {

		ResponseDTO responseDTO = userService.login(authorization);
		return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
	}
}
